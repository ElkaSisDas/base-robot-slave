/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * Author           : Muhammad Sholahuddin Al-Ayubi
  * Date             : 29 Februari 2020
  ******************************************************************************
  * CEPAT LULUS TUGAS AKHIR YA
  * PROGRESS--PROGRES--PROGRESS
  * JANGAN MAIN AJA
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "lcd.h"
#include "motor.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//========LCD=======//
char lcd[20];

//======Buzzer======//
char buzzer_status = 0;
short int buzzer_iterasi = 0;
short int buzzer_waktu = 0, buzzer_jumlah = 0;

//===Master-Slave===//
unsigned char data_terima2[23];
char data_kirim2[7] = {'i','t','s'};
char data_terima3[7];

//=====Kinematik====//
short int kecepatan_x;
short int kecepatan_y;
short int kecepatan_sudut;
float posisi_x;
float posisi_y;
float gyro_derajat;

//=======Misc========//
char status_control;
//char srf[3];
//unsigned char tombol_buffer;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
//======Buzzer======//
void buzzer(short int waktu, short int jumlah);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM9_Init();
  MX_TIM10_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  //============LCD============//
  lcd_init(20,4);

  //=========INTERRUPT=========//
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_TIM_Base_Start_IT(&htim7);

  //=========PWM MOTOR=========//
  HAL_TIM_PWM_Start(&htim9,TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim9,TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim10,TIM_CHANNEL_1);

  //=======ENCODER MOTOR=======//
  HAL_TIM_Encoder_Start(&htim1,TIM_CHANNEL_ALL);
  HAL_TIM_Encoder_Start(&htim2,TIM_CHANNEL_ALL);
  HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_ALL);

  //===========UART============//
  HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 23);//===>>>//UART2 Terima Master
  //HAL_UART_Transmit_DMA(&huart2, (uint8_t*)data_kirim2, 7);//====>>>//UART2 Kirim SRF
  //HAL_UART_Receive_DMA(&huart3, (uint8_t*)data_terima3, 7);//====>>>//UART3 Terima SRF

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  lcd_print(3,0,"LULUS TA AMIN");
  lcd_print(1,1,"BASE SERVICE ROBOT");
  lcd_print(7,2,"MARK I");
  lcd_print(2,3,"NO PAIN NO GAIN");
  buzzer(20,10);
  HAL_Delay(3000);
  lcd_clear();

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	lcd_print(14,0,"MODE:");
	if(status_control == 0)
	{
		lcd_print(14,1,"MANUAL");
	}
	else
	{
		lcd_print(14,1,"AUTO  ");
	}

	sprintf(lcd, "X=%+04d", (int)posisi_x);
	lcd_print(0,0,lcd);
	sprintf(lcd, "Y=%+04d", (int)posisi_y);
	lcd_print(0,1,lcd);
	sprintf(lcd, "A=%+04d", (int)gyro_derajat);
	lcd_print(0,2,lcd);

	sprintf(lcd, "VX=%+03d", (int)kecepatan_x);
	lcd_print(7,0,lcd);
	sprintf(lcd, "VY=%+03d", (int)kecepatan_y);
	lcd_print(7,1,lcd);
	sprintf(lcd, "W =%+03d", (int)kecepatan_sudut);
	lcd_print(7,2,lcd);

    sprintf(lcd, "w0=%3d", motor_velo[0]);
    lcd_print(0,3,lcd);
    sprintf(lcd, "w1=%3d", motor_velo[1]);
    lcd_print(7,3,lcd);
    sprintf(lcd, "w2=%3d", motor_velo[2]);
    lcd_print(14,3,lcd);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim-> Instance == TIM7)
	{
		motor_VeloControl();
	}

	if(htim-> Instance == TIM6)
	{
		motor_VectorKinematic(kecepatan_x,kecepatan_y,kecepatan_sudut);

		if (buzzer_status == 1 && buzzer_jumlah > 0 && buzzer_iterasi++ == buzzer_waktu)
		{
			HAL_GPIO_WritePin(buzzer_GPIO_Port, buzzer_Pin, GPIO_PIN_RESET);

			buzzer_iterasi = 0;
			buzzer_status = 0;
			buzzer_jumlah--;
		}
		else if (buzzer_status == 0 && buzzer_jumlah > 1 && buzzer_iterasi++ == buzzer_waktu)
		{
			HAL_GPIO_WritePin(buzzer_GPIO_Port, buzzer_Pin, GPIO_PIN_SET);

			buzzer_iterasi = 0;
			buzzer_status = 1;
			buzzer_jumlah--;
		}

	//	motor_status = !(tombol_buffer >> 7 & 1);
	}
}

/*
 * Fungsi dipanggil jika pengiriman data Tx UART Selesai
 */

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	/*
	 * Tx USART 2 Selesai Transmit
	 */

/*	if(huart->Instance == USART2)
	{
		memcpy(data_kirim2 + 3, &srf, 3);
		HAL_UART_Transmit_DMA(&huart2, (uint8_t*)data_kirim2, 7);
	}
*/
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART2)
	{
		if(data_terima2[0]=='i' && data_terima2[1]=='t' && data_terima2[2]=='s')
		{
			memcpy(&posisi_x, data_terima2 + 3, 4);
			memcpy(&posisi_y, data_terima2 + 7, 4);
			memcpy(&gyro_derajat, data_terima2 + 11, 4);
			memcpy(&kecepatan_x , data_terima2 + 15, 2);
			memcpy(&kecepatan_y , data_terima2 + 17, 2);
			memcpy(&kecepatan_sudut , data_terima2 + 19, 2);
			memcpy(&status_control , data_terima2 + 21, 1);

			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 23);
		}
		else
		{
			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 23);
		}
	}

/*	if(huart->Instance == USART3)
	{
		if(data_terima3[0]=='i' && data_terima3[1]=='t' && data_terima3[2]=='s')
		{
			memcpy(&data_kirim2, data_terima3, 7);
			memcpy(&srf, data_terima3 + 3, 3);

			HAL_UART_Receive_DMA(&huart3, (uint8_t*)data_terima3, 7);
		}
		else
		{
			HAL_UART_Receive_DMA(&huart3, (uint8_t*)data_terima3, 7);
		}
	}
*/
}

void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART2)
	{
		if(!(data_terima2[0]=='i' && data_terima2[1]=='t' && data_terima2[2]=='s'))
		{
			HAL_UART_AbortReceive(&huart2);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 23);
		}
	}

	if(huart->Instance == USART3)
	{
		if(!(data_terima3[0]=='i' && data_terima3[1]=='t' && data_terima3[2]=='s'))
		{
			HAL_UART_AbortReceive(&huart3);
			HAL_UART_Receive_DMA(&huart3, (uint8_t*)data_terima3, 7);
		}
	}
}

/*
 * Jika Komunikasi Serial gagal, maka akan diaktifkan kembali
 */

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART2)
	{
		HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 23);
		HAL_UART_Transmit_DMA(&huart2, (uint8_t*)data_kirim2, 7);
	}

	if(huart->Instance == USART3)
	{
		HAL_UART_Receive_DMA(&huart3, (uint8_t*)data_terima3, 7);
	}
}

/*
 * Buzzer Routine
 */

void buzzer(short int waktu, short int jumlah)
{
	buzzer_status = 1;
	buzzer_iterasi = 0;
	buzzer_waktu = waktu;
	buzzer_jumlah = jumlah * 2;

	HAL_GPIO_WritePin(buzzer_GPIO_Port, buzzer_Pin, GPIO_PIN_SET);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
