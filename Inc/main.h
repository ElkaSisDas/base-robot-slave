/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define buzzer_Pin GPIO_PIN_2
#define buzzer_GPIO_Port GPIOE
#define DirBMotor0_Pin GPIO_PIN_4
#define DirBMotor0_GPIO_Port GPIOE
#define PWMMotor0_Pin GPIO_PIN_5
#define PWMMotor0_GPIO_Port GPIOE
#define PWMMotor1_Pin GPIO_PIN_6
#define PWMMotor1_GPIO_Port GPIOE
#define DirAMotor0_Pin GPIO_PIN_13
#define DirAMotor0_GPIO_Port GPIOC
#define DirAMotor1_Pin GPIO_PIN_14
#define DirAMotor1_GPIO_Port GPIOC
#define DirBMotor1_Pin GPIO_PIN_15
#define DirBMotor1_GPIO_Port GPIOC
#define PH0_OSC_IN_Pin GPIO_PIN_0
#define PH0_OSC_IN_GPIO_Port GPIOH
#define PH1_OSC_OUT_Pin GPIO_PIN_1
#define PH1_OSC_OUT_GPIO_Port GPIOH
#define ENC0A_Pin GPIO_PIN_9
#define ENC0A_GPIO_Port GPIOE
#define ENC0B_Pin GPIO_PIN_11
#define ENC0B_GPIO_Port GPIOE
#define lcd_rs_Pin GPIO_PIN_13
#define lcd_rs_GPIO_Port GPIOB
#define lcd_e_Pin GPIO_PIN_15
#define lcd_e_GPIO_Port GPIOB
#define lcd_e2_Pin GPIO_PIN_8
#define lcd_e2_GPIO_Port GPIOD
#define lcd_db4_Pin GPIO_PIN_9
#define lcd_db4_GPIO_Port GPIOD
#define lcd_db5_Pin GPIO_PIN_11
#define lcd_db5_GPIO_Port GPIOD
#define LED_Pin GPIO_PIN_12
#define LED_GPIO_Port GPIOD
#define lcd_db6_Pin GPIO_PIN_13
#define lcd_db6_GPIO_Port GPIOD
#define lcd_db7_Pin GPIO_PIN_15
#define lcd_db7_GPIO_Port GPIOD
#define ENC1A_Pin GPIO_PIN_15
#define ENC1A_GPIO_Port GPIOA
#define DirAMotor2_Pin GPIO_PIN_1
#define DirAMotor2_GPIO_Port GPIOD
#define DirBMotor2_Pin GPIO_PIN_3
#define DirBMotor2_GPIO_Port GPIOD
#define ENC1B_Pin GPIO_PIN_3
#define ENC1B_GPIO_Port GPIOB
#define ENC2A_Pin GPIO_PIN_4
#define ENC2A_GPIO_Port GPIOB
#define ENC2B_Pin GPIO_PIN_5
#define ENC2B_GPIO_Port GPIOB
#define PWMMotor2_Pin GPIO_PIN_8
#define PWMMotor2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
