#ifndef MOTOR_H_
#define MOTOR_H_

#include "main.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#define PWM0 TIM9->CCR1
#define PWM1 TIM9->CCR2
#define PWM2 TIM10->CCR1

#define Encoder0 TIM1->CNT
#define Encoder1 TIM2->CNT
#define Encoder2 TIM3->CNT

#define KP_motor 20
#define KI_motor 0.5
#define KD_motor 0.2

#define output_max_velo_motor 999
#define output_min_velo_motor -999

#define integral_output_max 100
#define integral_output_min -100
#define derivative_output_max 100
#define derivative_output_min -100

short int motor_velo[3], motor_SetPoint[3];
char motor_status;

void motor_VectorKinematic(short int vx, short int vy, short int vsudut);
void motor_VeloControl(void);
void motor_SetPWM(short int outputPWM[4]);

#endif /* MOTOR_H_ */
