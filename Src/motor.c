#include "motor.h"

extern short int kecepatan_x;
extern short int kecepatan_y;
extern short int kecepatan_sudut;

void motor_VectorKinematic(short int vx, short int vy, short int vsudut)
{
	motor_SetPoint[0] = (short int) ((vx * cosf(120 * 0.0174533)) + (vy * sinf(120 * 0.0174533)) + vsudut);
	motor_SetPoint[1] = (short int) ((vx * cosf(240 * 0.0174533)) + (vy * sinf(240 * 0.0174533)) + vsudut);
	motor_SetPoint[2] = (short int) ((vx * cosf(0 * 0.0174533)) + (vy * sinf(0 * 0.0174533)) + vsudut);
}

void motor_VeloControl(void)
{
	float proportional_motor[3], integral_motor[3], derivative_motor[3];
	float error_velo_motor[3], sum_error_velo_motor[3], previous_error_velo_motor[3];
	short int output_velo_motor[3];

	motor_velo[0] = Encoder0;
	motor_velo[1] = Encoder1;
	motor_velo[2] = Encoder2;

	Encoder0=Encoder1=Encoder2=0;

	for(int i = 0; i < 3; i++)
	{
		error_velo_motor[i] = motor_SetPoint[i] - motor_velo[i];

		sum_error_velo_motor[i] += error_velo_motor[i];

		if(integral_motor[i] > integral_output_max) integral_motor[i] = integral_output_max;
		else if(integral_motor[i] < integral_output_min) integral_motor[i] = integral_output_min;

		if(derivative_motor[i] > derivative_output_max) derivative_motor[i] = derivative_output_max;
		else if(derivative_motor[i] < derivative_output_min) derivative_motor[i] = derivative_output_min;

		proportional_motor[i] = KP_motor * error_velo_motor[i];
		integral_motor[i] = KI_motor * sum_error_velo_motor[i];
		derivative_motor[i] = KD_motor * (error_velo_motor[i] - previous_error_velo_motor[i]);

		previous_error_velo_motor[i] = error_velo_motor[i];

		output_velo_motor[i] = (short int) proportional_motor[i] + integral_motor[i] + derivative_motor[i];

		if(output_velo_motor[i] > output_max_velo_motor) output_velo_motor[i] = output_max_velo_motor;
		else if(output_velo_motor[i] < output_min_velo_motor) output_velo_motor[i] = output_min_velo_motor;
	}

	if (motor_status)
		for (int i = 0; i < 3; i++)
		{
			proportional_motor[i] = 0;
			integral_motor[i] = 0;
			derivative_motor[i] = 0;
			error_velo_motor[i] = 0;
			sum_error_velo_motor[i] = 0;
			previous_error_velo_motor[i] = 0;
			output_velo_motor[i] = 0;
		}

	motor_SetPWM(output_velo_motor);
}

void motor_SetPWM(short int outputPWM[3])
{
	if (outputPWM[0] > 0)
	{
		HAL_GPIO_WritePin(DirAMotor0_GPIO_Port, DirAMotor0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DirBMotor0_GPIO_Port, DirBMotor0_Pin, GPIO_PIN_RESET);
	}
	else if (outputPWM[0] < 0)
	{
		HAL_GPIO_WritePin(DirAMotor0_GPIO_Port, DirAMotor0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DirBMotor0_GPIO_Port, DirBMotor0_Pin, GPIO_PIN_SET);
	}

//=============================================================================================

	if (outputPWM[1] > 0)
	{
		HAL_GPIO_WritePin(DirAMotor1_GPIO_Port, DirAMotor1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DirBMotor1_GPIO_Port, DirBMotor1_Pin, GPIO_PIN_RESET);
	}
	else if (outputPWM[1] < 0)
	{
		HAL_GPIO_WritePin(DirAMotor1_GPIO_Port, DirAMotor1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DirBMotor1_GPIO_Port, DirBMotor1_Pin, GPIO_PIN_SET);
	}

//=============================================================================================

	if (outputPWM[2] > 0)
	{
		HAL_GPIO_WritePin(DirAMotor2_GPIO_Port, DirAMotor2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DirBMotor2_GPIO_Port, DirBMotor2_Pin, GPIO_PIN_RESET);
	}
	else if (outputPWM[2] < 0)
	{
		HAL_GPIO_WritePin(DirAMotor2_GPIO_Port, DirAMotor2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DirBMotor2_GPIO_Port, DirBMotor2_Pin, GPIO_PIN_SET);
	}

	PWM0 = abs(outputPWM[0]);
	PWM1 = abs(outputPWM[1]);
	PWM2 = abs(outputPWM[2]);
}
